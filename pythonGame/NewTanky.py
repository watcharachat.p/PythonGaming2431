from gi import require_version
require_version('Atspi', '2.0')
from gi.repository import Atspi
import random
import arcade
import arcade.key
from jinja2.ext import WithExtension
from orca.punctuation_settings import bullet

SPRITE_SCALING = 1

SCREEN_WIDTH = 700
SCREEN_HEIGHT = 500

BULLET_SPEED = 5


class Bullet(arcade.Sprite):
    def update(self):
        if self.angle == 0:
            if self.center_y <= SCREEN_HEIGHT-10:
                self.center_y += 3

            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
                self.angle = 180
            if self.center_x >= SCREEN_WIDTH:
                self.center_x = SCREEN_WIDTH-10
            if self.center_y <= 10:
                self.center_y = 10
            if self.center_x <= 10:
                self.center_x = 10
                
        if self.angle == 90:
            if self.center_x >= 10:
                self.center_x -= 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
            if self.center_y <= 10:
                self.center_y = 10
            if self.center_x <= 10:
                self.center_x = 10
                self.angle = 270
                
        if self.angle == 180:
            if self.center_y >= 10:
                self.center_y -= 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
            if self.center_y <= 10:
                self.center_y = 10
                self.angle = 0
            if self.center_x <= 10:
                self.center_x = 10
                
        if self.angle == 270:
            if self.center_x <= SCREEN_WIDTH-10:
                self.center_x += 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
                self.angle = 90
            if self.center_y <= 10:
                self.center_y = 10
            if self.center_x <= 10:
                self.center_x = 10
                
        if self.angle == 45:
            if self.center_y <= SCREEN_HEIGHT-10:
                self.center_y += 3
            if self.center_x >= 10:
                self.center_x -= 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
                self.angle = 135
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
            if self.center_y <= 10:
                self.center_y = 10
            if self.center_x <= 10:
                self.center_x = 10
                self.angle = 315
                
        if self.angle == 135:
            if self.center_y >= 10:
                self.center_y -= 3
            if self.center_x >= 10:
                self.center_x -= 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
            if self.center_y <= 10:
                self.center_y = 10
                self.angle = 45
            if self.center_x <= 10:
                self.center_x = 10
                self.angle = 225
                
        if self.angle == 225:
            if self.center_y >= 10:
                self.center_y -= 3
            if self.center_x <= SCREEN_WIDTH-10:
                self.center_x += 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
                self.angle = 135
            if self.center_y <= 10:
                self.center_y = 10
                self.angle = 315
            if self.center_x <= 10:
                self.center_x = 10
                
        if self.angle == 315:
            if self.center_y <= SCREEN_HEIGHT-10:
                self.center_y += 3
            if self.center_x <= SCREEN_WIDTH-10:
                self.center_x += 3
                
            if self.center_y >= SCREEN_HEIGHT-10:
                self.center_y = SCREEN_HEIGHT-10
                self.angle = 225
            if self.center_x >= SCREEN_WIDTH-10:
                self.center_x = SCREEN_WIDTH-10
                self.angle = 45
            if self.center_y <= 10:
                self.center_y = 10
            if self.center_x <= 10:
                self.center_x = 10
                
class Tank(arcade.Sprite):
    DIR_FORWARD = 0
    DIR_TURNLEFT = 1
    DIR_TURNRIGHT = 2
    DIR_BACKWARD = 3
    direction = DIR_BACKWARD
 
    def update(self):
        if self.direction == Tank.DIR_FORWARD:
            if self.angle == 0:
                if self.center_y <= SCREEN_HEIGHT-10:
                    self.center_y += 2
            if self.angle == 90:
                if self.center_x >= 10:
                    self.center_x -= 2
            if self.angle == 180:
                if self.center_y >= 10:
                    self.center_y -= 2
            if self.angle == 270:
                if self.center_x <= SCREEN_WIDTH-10:
                    self.center_x += 2
            if self.angle == 45:
                if self.center_y <= SCREEN_HEIGHT-10:
                    self.center_y += 2
                if self.center_x >= 10:
                    self.center_x -= 2
            if self.angle == 135:
                if self.center_y >= 10:
                    self.center_y -= 2
                if self.center_x >= 10:
                    self.center_x -= 2
            if self.angle == 225:
                if self.center_y >= 10:
                    self.center_y -= 2
                if self.center_x <= SCREEN_WIDTH-10:
                    self.center_x += 2
            if self.angle == 315:
                if self.center_y <= SCREEN_HEIGHT-10:
                    self.center_y += 2
                if self.center_x <= SCREEN_WIDTH-10:
                    self.center_x += 2
        if self.direction == Tank.DIR_TURNRIGHT:
            self.angle += 315
            self.angle %= 360
            self.direction = Tank.DIR_FORWARD
        if self.direction == Tank.DIR_TURNLEFT:
            self.angle += 45
            self.angle %= 360
            self.direction = Tank.DIR_FORWARD
        if self.center_y >= SCREEN_HEIGHT:
            self.center_y = SCREEN_HEIGHT-10
        if self.center_x >= SCREEN_WIDTH:
            self.center_x = SCREEN_WIDTH-10
        if self.center_y <= 10:
            self.center_y = 10
        if self.center_x <= 10:
            self.center_x = 10
            


class MyAppWindow(arcade.Window):
    """ Main application class. """

    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Sprites and Bullets Demo")

        """ Set up the game and initialize the variables. """

        # Sprite lists
        self.all_sprites_list = arcade.SpriteList()
        self.player1_list = arcade.SpriteList()
        self.player2_list = arcade.SpriteList()
        self.bullet_list = arcade.SpriteList()

        # Set up the player
        self.score1 = 25
        self.score2 = 25
        self.tank1 = Tank("images/tank1.png",SPRITE_SCALING)
        self.tank2 = Tank("images/tank2.png",SPRITE_SCALING)
        
        self.tank1.center_x = 100
        self.tank1.center_y = 100
        self.tank1.angle = 270
        self.all_sprites_list.append(self.tank1)
        self.player1_list.append(self.tank1)
        
        self.tank2.center_x = 600
        self.tank2.center_y = 400
        self.tank2.angle = 90
        self.all_sprites_list.append(self.tank2)
        self.player2_list.append(self.tank2)

        # Don't show the mouse cursor
        self.set_mouse_visible(False)

        # Set the background color
        arcade.set_background_color(arcade.color.BLACK)
        
        self.IsGameEnd = True
        
        #self.gun_sound = arcade.sound.load_sound("images/gunshot.ogg")

    def on_draw(self):
        """
        Render the screen.
        """

        # This command has to happen before we start drawing
        arcade.start_render()

        # Draw all the sprites.
        self.all_sprites_list.draw()

        # Put the text on the screen.
        output1 = "P1 HP: {}".format(self.score1)
        arcade.draw_text(output1, 10, 40, arcade.color.RED, 14)
        
        output4 = "Use W A S D and F"
        arcade.draw_text(output4, 10, 20, arcade.color.RED, 10)
         
        output2 = "P2 HP: {}".format(self.score2)
        arcade.draw_text(output2, 600, 40, arcade.color.GREEN, 14)
        
        output5 = "Use UP DOWN LEFT RIGHT and /"
        arcade.draw_text(output5, 480, 20, arcade.color.GREEN, 10)
        
        
        if(self.score1 <= 0):
            self.IsGameEnd = False
            output3 = "P2 WINS"
            arcade.draw_text(output3, 275, 250, arcade.color.GREEN, 30)
        if(self.score2 <= 0):
            self.IsGameEnd = False
            output3 = "P1 WINS"
            arcade.draw_text(output3, 275, 250, arcade.color.RED, 30)

    def on_key_press(self, key, key_modifiers):
        # Create a bullet
        if key == arcade.key.F:
            #self.gun_sound.play()
            bullet = Bullet("images/bullet.png", SPRITE_SCALING * 1.5)
        # The image points to the right, and we want it to point up. So
        # rotate it.
            bullet.angle = self.tank1.angle
        # Position the bullet
            offsetx = 0
            offsety = 0
            if bullet.angle == 0:
                offsety += 40
            if bullet.angle == 90:
                offsetx -= 40
            if bullet.angle == 180:
                offsety -= 40
            if bullet.angle == 270:
                offsetx += 40
            if bullet.angle == 45:
                offsety += 40
                offsetx -= 40
            if bullet.angle == 135:
                offsety -= 40
                offsetx -= 40
            if bullet.angle == 225:
                offsety -= 40
                offsetx += 40
            if bullet.angle == 315:
                offsety += 40
                offsetx += 40
            bullet.center_x = self.tank1.center_x+offsetx
            bullet.center_y = self.tank1.center_y+offsety
        # Add the bullet to the appropriate lists
            self.all_sprites_list.append(bullet)
            self.bullet_list.append(bullet)
        if key == arcade.key.SLASH:
            #self.gun_sound.play()
            bullet = Bullet("images/bullet.png", SPRITE_SCALING * 1.5)
        # The image points to the right, and we want it to point up. So
        # rotate it.
            bullet.angle = self.tank2.angle
        # Position the bullet
            offsetx = 0
            offsety = 0
            if bullet.angle == 0:
                offsety += 40
            if bullet.angle == 90:
                offsetx -= 40
            if bullet.angle == 180:
                offsety -= 40
            if bullet.angle == 270:
                offsetx += 40
            if bullet.angle == 45:
                offsety += 40
                offsetx -= 40
            if bullet.angle == 135:
                offsety -= 40
                offsetx -= 40
            if bullet.angle == 225:
                offsety -= 40
                offsetx += 40
            if bullet.angle == 315:
                offsety += 40
                offsetx += 40
            bullet.center_x = self.tank2.center_x+offsetx
            bullet.center_y = self.tank2.center_y+offsety
        # Add the bullet to the appropriate lists
            self.all_sprites_list.append(bullet)
            self.bullet_list.append(bullet)
            
            
        if key == arcade.key.W:
            self.tank1.direction = (Tank.DIR_FORWARD)
        if key == arcade.key.S:
            self.tank1.direction = (Tank.DIR_BACKWARD)
        if key == arcade.key.A:
            self.tank1.direction = (Tank.DIR_TURNLEFT)
        if key == arcade.key.D:
            self.tank1.direction = (Tank.DIR_TURNRIGHT)
            
        if key == arcade.key.UP:
            self.tank2.direction = (Tank.DIR_FORWARD)
        if key == arcade.key.DOWN:
            self.tank2.direction = (Tank.DIR_BACKWARD)
        if key == arcade.key.LEFT:
            self.tank2.direction = (Tank.DIR_TURNLEFT)
        if key == arcade.key.RIGHT:
            self.tank2.direction = (Tank.DIR_TURNRIGHT)
        

    def animate(self, delta_time):
        """ Movement and game logic """

        # Call update on all sprites (The sprites don't do much in this
        # example though.)
        self.all_sprites_list.update()

        # Loop through each bullet
        # Check this bullet to see if it hit a coin
        hit_list1 = arcade.check_for_collision_with_list(self.tank1,
                                                            self.bullet_list)
        hit_list2 = arcade.check_for_collision_with_list(self.tank2,
                                                            self.bullet_list)
            # If it did, get rid of the bullet
        for bullet in hit_list1:
                bullet.kill()
                if(self.IsGameEnd):
                    self.score1 -= 1
                 
        for bullet in hit_list2:
                bullet.kill()
                if(self.IsGameEnd):
                    self.score2 -= 1
                


def main():
    MyAppWindow()
    arcade.run()


if __name__ == "__main__":
    main()